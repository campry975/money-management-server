const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const {check, validationResult} = require('express-validator');
const Transaction = require('../models/Transaction');
const Contact = require('../models/Contact');

router.get('/', auth, async (req, res) => {
  try {
    await Transaction.find({
      user: req.user.id
    }).populate('contacts')
    .exec((err, transactions) => {
      res.json(transactions);
    });
  } catch (e) {
    console.error(e.message);
    res.status(500).send('Server Error');
  }
});

router.get('/:id', auth, async (req, res) => {
  try {
    const transaction = await Transaction.findOne({_id: req.params.id});
    if (!transaction) {
      return res.status(400).send({
        error: `Transaction not found with id ${req.params.id}`
      })
    }
  } catch (e) {
    console.log(e);
    return res.status(500).send({
      error: `Error getting transaction with id ${req.params.id}`
    })
  }
});

router.post('/', [auth, [
  check('value', 'Please add transaction value').isLength({min: 1}).not().isEmpty(),
  check('contacts', 'Please add at least one contact').not().isEmpty()
]], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) return res.status(400).json({errors: errors.array()});

  const {value, contacts, notes, debtors} = req.body;
  console.log(debtors)
  try {

    const transaction = new Transaction({
      user: req.user.id,
      value,
      notes,
      contacts,
      debtors: JSON.stringify(debtors)
    });
    
    console.log(transaction)

    await transaction.save(async (err, transaction) => {
      if (err) {
        console.log(err);
      }
      // TODO: PARSE DON'T WORK ???????
      transaction.debtors = JSON.parse(transaction.debtors);
      transaction.contacts = await Contact.find({_id: contacts});

      res.json(transaction);
    });


  } catch (e) {
    console.error(e.message);
    res.status(500).json({error: 'Server Error'});
  }
});

router.put('/:id', [auth, [
  check('value', 'Please add transaction value').isLength({min: 1}).not().isEmpty(),
  check('contacts', 'Please add at least one contact').not().isEmpty()
]], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({errors: errors.array()});
  }
  const {value, contacts, active, debtors, notes} = req.body;
  try {
    const updatedTransaction = await Transaction.findOneAndUpdate({_id: req.params.id}, {
      value,
      contacts,
      notes,
      active,
      debtors: JSON.stringify(debtors)
    }, {new: true}).populate('contacts');

    res.json(updatedTransaction);
  } catch (e) {
    console.log(e);
    return res.status(500).send({
      error: `Error updating transaction with id ${req.params.id}`
    })
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    await Transaction.findOneAndDelete(req.params.id, () => {
      res.status(200).send();
    })
  } catch (e) {
    res.status(500).send({
      error: "Could not delete transaction with id " + req.params.id
    });
  }
});

module.exports = router;
