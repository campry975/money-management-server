const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const {check, validationResult} = require('express-validator');

const User = require('../models/User');
const Contact = require('../models/Contact');
const Transaction = require('../models/Transaction');


/**
 * @route GET api/contacts
 * @desc Get all users contacts
 * @access Private
 */
router.get('/', auth, async (req, res) => {
  try {
    const contacts = await Contact.find({user: req.user.id}).populate('transaction').sort({date: -1});
    res.json(contacts)
  } catch (e) {
    console.error(e.message);
    res.status(500).send('Server Error');
  }
});

/**
 * @route POST api/contacts
 * @desc Create contact
 * @access Private
 */
router.post('/', [auth, [
  check('firstName', 'Name is required ')
]], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) return res.status(400).json({errors: errors.array()});

  const {firstName, lastName, phone, backgroundColor} = req.body;
  try {
    const newContact = new Contact({
      firstName,
      lastName,
      phone,
      backgroundColor,
      user: req.user.id
    });
    const contact = await newContact.save();

    res.json(contact);
  } catch (e) {
    console.error(e.message);
    res.status(500).send('Server Error');
  }
});

/**
 * @route PUT api/contacts/:id
 * @desc Update contact
 * @access Private
 */
router.put('/:id', [auth, [
  check('firstName', 'Name is required')
]], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({errors: errors.array()});
  }
  const {firstName, lastName, phone} = req.body;
  try {
    await Contact.findOneAndUpdate({_id: req.params.id}, {
      firstName,
      lastName,
      phone
    }, {new: true});
    res.status(200).send();
  } catch (e) {
    console.log(e);
    return res.status(500).send({
      error: `Error updating contact with id ${req.params.id}`
    })
  }
});

router.get('/:id', async (req, res) => {
  try {
    const contact = await Contact.findOne({_id: req.params.id});
    contact.transactions = await Transaction.find({contacts: req.params.id});
    res.json(contact);
  } catch (e) {
    console.error(e.message);
    res.status(500).send('Server Error');
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    await Contact.findOneAndRemove(req.params.id, () => {
      res.status(200).send()
    });

  } catch (e) {
    res.status(500).send({
      error: "Could not delete contact with id " + req.params.id
    });
  }
});


module.exports = router;
