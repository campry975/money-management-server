const mongoose = require('mongoose');
const config = require('config');
const db = config.get('mongoURI');

const connectDB = async () => {
  try {
    await mongoose.connect(db, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false
    });
    console.log('Connected to MongoDB')
  } catch (e) {
    console.log(e);
    process.exit(1);
  }
};

// "mongoURI": "mongodb+srv://campry:qkfryt20@moneymanagement-vnrb0.mongodb.net/test?retryWrites=true&w=majority",
// "mongoURI": "mongodb://heroku_tslbjp37:8hbjh05m45h38qv21gq5a3055v@ds257507.mlab.com:57507/heroku_tslbjp37",

module.exports = connectDB;
