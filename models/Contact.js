const mongoose = require('mongoose');
const Transaction = require('./Transaction');
let Schema = mongoose.Schema;

const ContactSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users'
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  backgroundColor: {
    type: String
  },
  phone: {
    type: String
  }
});

module.exports = mongoose.model('Contact', ContactSchema);
