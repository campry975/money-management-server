const mongoose = require('mongoose');
const Contact = require('./Contact');

const TransactionSchema = mongoose.Schema({
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users'
    },
    value: {
      type: String,
      required: true
    },
    notes: {
      type: String,
    },
    active: {
      type: Boolean,
      default: true
    },
    contacts: [{type: mongoose.Schema.ObjectId, ref: 'Contact'}],
    debtors: {
      type: String
    }
  }
);

TransactionSchema.set('timestamps', true);

module.exports = mongoose.model('Transaction', TransactionSchema);
